import { Component, OnInit } from '@angular/core';

interface Data{
   departamento: string[];
   numeroHabitantes: number[];
   titulo: string;
}

@Component({
  selector: 'app-grafica1',
  templateUrl: './grafica1.component.html',
  styles: [
  ]
})
export class Grafica1Component implements OnInit {

  colors= [ '#9E120E','#FF5800','#FFB414'];
  titleOne = 'Grafico uno';
  titleTwo = 'Grafico dos';
  titleThree = 'Grafico tres';
  titleFour = 'Grafico cuatro';

  labels = {
    labelsOne:  ['Pan', 'Azúcar'],
    labelsTwo:  ['Aceite', 'Harina'],
    labelsThree:  ['Pescado', 'Pollo'],
    labelsFour:  ['Leche light', 'Leche entera'],
  }
  dataOne: number[] = [ 100, 20 ] ;
  dataTwo: number[] = [ 34, 123 ] ;
  dataThree: number[] = [ 100, 20 ] ;
  dataFour: number[] = [ 203, 30 ] ;

  constructor() { }

  ngOnInit(): void {



  }




}
