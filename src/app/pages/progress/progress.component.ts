import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-progress',
  templateUrl: './progress.component.html',
  styleUrls: ['./progress.component.css']
})
export class ProgressComponent implements OnInit {

  progressOne: number = 30;
  progressTwo: number = 50;

  constructor() { }

  get getPercentOne(){
    return `${this.progressOne}%`;
  }

  get getPercentTwo(){
    return `${this.progressTwo}%`;
  }

  ngOnInit(): void {
  }

}
