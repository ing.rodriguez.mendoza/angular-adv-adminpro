import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../../services/settings.service';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styles: [
  ]
})
export class AccountSettingsComponent implements OnInit {

  // public links: NodeListOf<Element> | undefined;

  constructor(private settingsService: SettingsService) {
    // console.log('links', this.links);
  }

  ngOnInit(): void {
    // console.log('links init', this.links);

    this.checkCurrenTheme();
  }

  changeTheme(theme: string, event: any){
      // const url = `./assets/css/colors/${theme}.css`;
      //console.log('event',event);
      this.settingsService.changeTheme(theme, event);


  }

  checkCurrenTheme(){
      this.settingsService.checkCurrenTheme();
  }

}
