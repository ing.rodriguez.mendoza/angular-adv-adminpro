import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// pages
import { DashboardComponent } from './dashboard/dashboard.component';
import { Grafica1Component } from './grafica1/grafica1.component';
import { PagesComponent } from './pages.component';
import { ProgressComponent } from './progress/progress.component';
import { AccountSettingsComponent } from './account-settings/account-settings.component';

const routes: Routes = [
  {
    path:'',
    component: PagesComponent,
    children: [
      { path:'', component: DashboardComponent },
      { path:'grafica', component: Grafica1Component },
      { path:'progress', component: ProgressComponent },
      { path:'account-settings', component: AccountSettingsComponent },
    ],
  },
];

@NgModule({
  declarations: [],
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [ RouterModule ]
})
export class PagesRoutingModule { }
