import { Component, OnInit } from '@angular/core';
import { SettingsService } from '../services/settings.service';

@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styles: [
  ]
})
export class PagesComponent implements OnInit {

  public link = document.querySelector('#theme');
  public theme =  localStorage.getItem('theme') ;

  constructor(private settingsService: SettingsService) {

  }

  ngOnInit(): void {

    this.link?.setAttribute('href',this.theme!);

  }

}
