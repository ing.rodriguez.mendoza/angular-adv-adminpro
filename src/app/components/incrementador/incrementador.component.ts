import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-incrementador',
  templateUrl: './incrementador.component.html',
  styles: [`
      .is-red {
        border-color: #dc3545;
      }
  `
  ]
})
export class IncrementadorComponent implements OnInit {

  @Input() progress!: number;
  @Input() btnClass: string = 'btn-primary';

  @Output() onGetPercent: EventEmitter<number> = new EventEmitter();

  constructor() { }

  ngOnInit(): void {
    this.btnClass = `btn ${this.btnClass}`;
  }


  changeValue(valor: number ){
    console.log('valor', valor);
    console.log('progress', this.progress);


    if (this.progress >= 100 && valor >= 0) {
      this.onGetPercent.emit(100);
      return //this.progress = 100;
    }

    if (this.progress <= 0 && valor < 0) {
      this.onGetPercent.emit(0);
      return //this.progress = 0;
    }

    this.progress += valor;
    return this.onGetPercent.emit(this.progress);

  }


  onChange(e: any){
      console.log( e );
      this.onGetPercent.emit(e);
      if (e >= 100) {
        this.onGetPercent.emit(100);
        return //this.progress = 100;
      }

      if (e <= 0) {
        this.onGetPercent.emit(0);
        return //this.progress = 0;
      }
      this.onGetPercent.emit(this.progress);

  }

}
