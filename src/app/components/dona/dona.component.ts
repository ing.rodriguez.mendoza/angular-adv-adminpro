import { AfterViewInit, Component, Input, OnChanges, OnInit, SimpleChanges } from '@angular/core';
import { ChartData, ChartType, } from 'chart.js';

@Component({
  selector: 'app-dona',
  templateUrl: './dona.component.html',
  styles: [
  ]
})
export class DonaComponent implements OnInit, AfterViewInit, OnChanges {

    @Input() labels: string[]= ['Download Sales', 'In-Store Sales'];
    @Input() data: number[]= [ 30, 20 ] ;
    @Input() title!: string ;
    @Input() colors!: string[];

    public doughnutChartData!: ChartData<'doughnut'>;

    // Doughnut
    public doughnutChartLabels: string[] = [ 'Download Sales', 'In-Store Sales'];

  constructor() { }
  ngOnChanges(changes: SimpleChanges): void {
    // throw new Error('Method not implemented.');
    console.log('ngOnChanges');

  }
  ngAfterViewInit(): void {
    console.log('ngAfterViewInit');

  }

  public doughnutChartType: ChartType = 'doughnut';

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  ngOnInit(): void {
    console.log('ngOnInit');

    this.doughnutChartData= {
      labels: this.labels,
      datasets: [
        {
          data: this.data,
          backgroundColor:  this.colors,
          hoverBackgroundColor:  this.colors,
          hoverBorderColor:  this.colors
        },
        // { data: [ 50, 150 ]   , backgroundColor:   [ '#9E120E','#FF5800','#FFB414'], hoverBackgroundColor:   [ '#9E120E','#FF5800','#FFB414'],  hoverBorderColor:   [ '#9E120E','#FF5800','#FFB414'] },
        // { data: [ 250, 130 ]  , backgroundColor:   [ '#9E120E','#FF5800','#FFB414'], hoverBackgroundColor:   [ '#9E120E','#FF5800','#FFB414'],  hoverBorderColor:   [ '#9E120E','#FF5800','#FFB414'] , label:'FFFDDD'}
      ]
    };

  }

}
