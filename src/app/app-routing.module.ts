import { NgModule } from '@angular/core';
import {  RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

import { NotPageFoundComponent } from './not-page-found/not-page-found.component';


const routes: Routes = [
  {
    path:'dashboard',
    loadChildren: () => import('./pages/pages.module').then(m=> m.PagesModule)
  },
  {
    path:'auth',
    loadChildren: () => import('./auth/auth.module').then(m=> m.AuthModule)
  },

  { path:'', redirectTo: '/dashboard', pathMatch: 'full' },
  { path:'**', component: NotPageFoundComponent },
]


@NgModule({
  declarations: [],
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
