import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SettingsService {

  private link = document.querySelector('#theme');

  constructor() {
        // console.log('hola desde mi servicio');
  }

  changeTheme(theme: string, event: any){
    const url = `./assets/css/colors/${theme}.css`;

    this.link?.setAttribute('href',url);
    // console.log('url', url);
    localStorage.setItem('theme', url);

    this.checkCurrenTheme(event)

  }

  checkCurrenTheme( event?: any ){
    // console.log(' event', event);

    if (!event) {
              // console.log('no event', event);
              const links = document.querySelectorAll('.selector');
              links?.forEach(
                elem => {
                  //removemos la clase working
                  elem.classList.remove('working');
                  //obtenemos el valor del atributo data-theme
                  const themeSelector =  `./assets/css/colors/${elem.getAttribute('data-theme')}.css`;
                  const themeLink =  this.link?.getAttribute('href');

                  if (themeSelector === themeLink) {
                      // console.log('son iguales');
                      elem.classList.add('working');
                  }
                  // console.log('themeSelector', themeSelector);
                  // console.log('themeLink', themeLink);
                }
          )
    } else {
      const links =  event.target.parentNode.parentNode.childNodes;
      // console.log('links event', links);

      links.forEach( (elm: any) => {
        // console.log('elm', elm.childNodes[0]);

        elm.childNodes[0].classList.remove('working');
      } );

              //obtenemos el valor del atributo data-theme
              const themeSelector =  `./assets/css/colors/${event.target.getAttribute('data-theme')}.css`;
              const themeLink =  this.link?.getAttribute('href');

              if (themeSelector === themeLink) {
                  // console.log('son iguales');
                  event.target.classList.add('working');

              }


    }



}

}
`¡
